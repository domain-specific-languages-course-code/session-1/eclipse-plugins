/**
 * generated by Xtext 2.13.0
 */
package com.javadude.dsl5.gothicSecurityDsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.javadude.dsl5.gothicSecurityDsl.Action#getCommand <em>Command</em>}</li>
 * </ul>
 *
 * @see com.javadude.dsl5.gothicSecurityDsl.GothicSecurityDslPackage#getAction()
 * @model
 * @generated
 */
public interface Action extends EObject
{
  /**
   * Returns the value of the '<em><b>Command</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Command</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Command</em>' reference.
   * @see #setCommand(Command)
   * @see com.javadude.dsl5.gothicSecurityDsl.GothicSecurityDslPackage#getAction_Command()
   * @model
   * @generated
   */
  Command getCommand();

  /**
   * Sets the value of the '{@link com.javadude.dsl5.gothicSecurityDsl.Action#getCommand <em>Command</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Command</em>' reference.
   * @see #getCommand()
   * @generated
   */
  void setCommand(Command value);

} // Action
