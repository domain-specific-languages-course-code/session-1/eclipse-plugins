/**
 * generated by Xtext 2.13.0
 */
package com.javadude.dsl5.gothicSecurityDsl.impl;

import com.javadude.dsl5.gothicSecurityDsl.Command;
import com.javadude.dsl5.gothicSecurityDsl.Event;
import com.javadude.dsl5.gothicSecurityDsl.GothicSecurityDslPackage;
import com.javadude.dsl5.gothicSecurityDsl.Reset;
import com.javadude.dsl5.gothicSecurityDsl.State;
import com.javadude.dsl5.gothicSecurityDsl.StateMachine;
import com.javadude.dsl5.gothicSecurityDsl.Test;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State Machine</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.javadude.dsl5.gothicSecurityDsl.impl.StateMachineImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.javadude.dsl5.gothicSecurityDsl.impl.StateMachineImpl#getStartState <em>Start State</em>}</li>
 *   <li>{@link com.javadude.dsl5.gothicSecurityDsl.impl.StateMachineImpl#getCommands <em>Commands</em>}</li>
 *   <li>{@link com.javadude.dsl5.gothicSecurityDsl.impl.StateMachineImpl#getEvents <em>Events</em>}</li>
 *   <li>{@link com.javadude.dsl5.gothicSecurityDsl.impl.StateMachineImpl#getStates <em>States</em>}</li>
 *   <li>{@link com.javadude.dsl5.gothicSecurityDsl.impl.StateMachineImpl#getResetEvents <em>Reset Events</em>}</li>
 *   <li>{@link com.javadude.dsl5.gothicSecurityDsl.impl.StateMachineImpl#getTests <em>Tests</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StateMachineImpl extends MinimalEObjectImpl.Container implements StateMachine
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getStartState() <em>Start State</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStartState()
   * @generated
   * @ordered
   */
  protected State startState;

  /**
   * The cached value of the '{@link #getCommands() <em>Commands</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCommands()
   * @generated
   * @ordered
   */
  protected EList<Command> commands;

  /**
   * The cached value of the '{@link #getEvents() <em>Events</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEvents()
   * @generated
   * @ordered
   */
  protected EList<Event> events;

  /**
   * The cached value of the '{@link #getStates() <em>States</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStates()
   * @generated
   * @ordered
   */
  protected EList<State> states;

  /**
   * The cached value of the '{@link #getResetEvents() <em>Reset Events</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getResetEvents()
   * @generated
   * @ordered
   */
  protected EList<Reset> resetEvents;

  /**
   * The cached value of the '{@link #getTests() <em>Tests</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTests()
   * @generated
   * @ordered
   */
  protected EList<Test> tests;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected StateMachineImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return GothicSecurityDslPackage.Literals.STATE_MACHINE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, GothicSecurityDslPackage.STATE_MACHINE__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public State getStartState()
  {
    if (startState != null && startState.eIsProxy())
    {
      InternalEObject oldStartState = (InternalEObject)startState;
      startState = (State)eResolveProxy(oldStartState);
      if (startState != oldStartState)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, GothicSecurityDslPackage.STATE_MACHINE__START_STATE, oldStartState, startState));
      }
    }
    return startState;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public State basicGetStartState()
  {
    return startState;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStartState(State newStartState)
  {
    State oldStartState = startState;
    startState = newStartState;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, GothicSecurityDslPackage.STATE_MACHINE__START_STATE, oldStartState, startState));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Command> getCommands()
  {
    if (commands == null)
    {
      commands = new EObjectContainmentEList<Command>(Command.class, this, GothicSecurityDslPackage.STATE_MACHINE__COMMANDS);
    }
    return commands;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Event> getEvents()
  {
    if (events == null)
    {
      events = new EObjectContainmentEList<Event>(Event.class, this, GothicSecurityDslPackage.STATE_MACHINE__EVENTS);
    }
    return events;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<State> getStates()
  {
    if (states == null)
    {
      states = new EObjectContainmentEList<State>(State.class, this, GothicSecurityDslPackage.STATE_MACHINE__STATES);
    }
    return states;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Reset> getResetEvents()
  {
    if (resetEvents == null)
    {
      resetEvents = new EObjectContainmentEList<Reset>(Reset.class, this, GothicSecurityDslPackage.STATE_MACHINE__RESET_EVENTS);
    }
    return resetEvents;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Test> getTests()
  {
    if (tests == null)
    {
      tests = new EObjectContainmentEList<Test>(Test.class, this, GothicSecurityDslPackage.STATE_MACHINE__TESTS);
    }
    return tests;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case GothicSecurityDslPackage.STATE_MACHINE__COMMANDS:
        return ((InternalEList<?>)getCommands()).basicRemove(otherEnd, msgs);
      case GothicSecurityDslPackage.STATE_MACHINE__EVENTS:
        return ((InternalEList<?>)getEvents()).basicRemove(otherEnd, msgs);
      case GothicSecurityDslPackage.STATE_MACHINE__STATES:
        return ((InternalEList<?>)getStates()).basicRemove(otherEnd, msgs);
      case GothicSecurityDslPackage.STATE_MACHINE__RESET_EVENTS:
        return ((InternalEList<?>)getResetEvents()).basicRemove(otherEnd, msgs);
      case GothicSecurityDslPackage.STATE_MACHINE__TESTS:
        return ((InternalEList<?>)getTests()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case GothicSecurityDslPackage.STATE_MACHINE__NAME:
        return getName();
      case GothicSecurityDslPackage.STATE_MACHINE__START_STATE:
        if (resolve) return getStartState();
        return basicGetStartState();
      case GothicSecurityDslPackage.STATE_MACHINE__COMMANDS:
        return getCommands();
      case GothicSecurityDslPackage.STATE_MACHINE__EVENTS:
        return getEvents();
      case GothicSecurityDslPackage.STATE_MACHINE__STATES:
        return getStates();
      case GothicSecurityDslPackage.STATE_MACHINE__RESET_EVENTS:
        return getResetEvents();
      case GothicSecurityDslPackage.STATE_MACHINE__TESTS:
        return getTests();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case GothicSecurityDslPackage.STATE_MACHINE__NAME:
        setName((String)newValue);
        return;
      case GothicSecurityDslPackage.STATE_MACHINE__START_STATE:
        setStartState((State)newValue);
        return;
      case GothicSecurityDslPackage.STATE_MACHINE__COMMANDS:
        getCommands().clear();
        getCommands().addAll((Collection<? extends Command>)newValue);
        return;
      case GothicSecurityDslPackage.STATE_MACHINE__EVENTS:
        getEvents().clear();
        getEvents().addAll((Collection<? extends Event>)newValue);
        return;
      case GothicSecurityDslPackage.STATE_MACHINE__STATES:
        getStates().clear();
        getStates().addAll((Collection<? extends State>)newValue);
        return;
      case GothicSecurityDslPackage.STATE_MACHINE__RESET_EVENTS:
        getResetEvents().clear();
        getResetEvents().addAll((Collection<? extends Reset>)newValue);
        return;
      case GothicSecurityDslPackage.STATE_MACHINE__TESTS:
        getTests().clear();
        getTests().addAll((Collection<? extends Test>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case GothicSecurityDslPackage.STATE_MACHINE__NAME:
        setName(NAME_EDEFAULT);
        return;
      case GothicSecurityDslPackage.STATE_MACHINE__START_STATE:
        setStartState((State)null);
        return;
      case GothicSecurityDslPackage.STATE_MACHINE__COMMANDS:
        getCommands().clear();
        return;
      case GothicSecurityDslPackage.STATE_MACHINE__EVENTS:
        getEvents().clear();
        return;
      case GothicSecurityDslPackage.STATE_MACHINE__STATES:
        getStates().clear();
        return;
      case GothicSecurityDslPackage.STATE_MACHINE__RESET_EVENTS:
        getResetEvents().clear();
        return;
      case GothicSecurityDslPackage.STATE_MACHINE__TESTS:
        getTests().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case GothicSecurityDslPackage.STATE_MACHINE__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case GothicSecurityDslPackage.STATE_MACHINE__START_STATE:
        return startState != null;
      case GothicSecurityDslPackage.STATE_MACHINE__COMMANDS:
        return commands != null && !commands.isEmpty();
      case GothicSecurityDslPackage.STATE_MACHINE__EVENTS:
        return events != null && !events.isEmpty();
      case GothicSecurityDslPackage.STATE_MACHINE__STATES:
        return states != null && !states.isEmpty();
      case GothicSecurityDslPackage.STATE_MACHINE__RESET_EVENTS:
        return resetEvents != null && !resetEvents.isEmpty();
      case GothicSecurityDslPackage.STATE_MACHINE__TESTS:
        return tests != null && !tests.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //StateMachineImpl
