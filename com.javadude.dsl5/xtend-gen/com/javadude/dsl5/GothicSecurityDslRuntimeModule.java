/**
 * generated by Xtext 2.13.0
 */
package com.javadude.dsl5;

import com.javadude.dsl5.AbstractGothicSecurityDslRuntimeModule;

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
@SuppressWarnings("all")
public class GothicSecurityDslRuntimeModule extends AbstractGothicSecurityDslRuntimeModule {
}
